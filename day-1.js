const numbers = require("./src/day-1");
const list = (numbers).split("");

let result = 0;

// Part one
for (let i = 0, len = list.length; i < len; i++) {
  result += (list[i] == list[(i + 1 >= list.length) ? 0 : i + 1]) ? +list[i] : 0;
}

// Part two
for (let i = 0, len = list.length; i < len; i++) {
  result += (list[i] == list[(i+(list.length / 2)) % list.length]) ? +list[i] : 0;
}
