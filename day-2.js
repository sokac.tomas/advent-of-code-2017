const numbers = require("./src/day-2");
const columns = numbers.split("\n");
let result = 0;

// Part one
for (let col = 0, cols = columns.length; col < cols ;col++) {
  let row = columns[col].split("\t");
  result += (Math.max(...row) - Math.min(...row));
}

// Part two
for (let col = 1, cols = columns.length - 1; col < cols ;col++) {
  let row = columns[col].split("\t");

  for (let i = 0, first = row.length; i < first; i++) {
    let max = Math.max(...row);
    delete row.splice(row.indexOf(""+max), 1);

    for (let j = 0, second = row.length; j < second; j++) {
      let num = +row[j];
      result += (max % num == 0) ? (max / num) : 0;
    }
  }
}
